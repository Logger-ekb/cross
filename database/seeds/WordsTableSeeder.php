<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WordsTableSeeder extends Seeder
{
    public function run()
    {
        // feelings
        $topic = DB::select('select id from topics where name = "feelings" limit 1');
        $this->addWordWithRelation($topic, 'amazed');
        $this->addWordWithRelation($topic, 'amused');
        $this->addWordWithRelation($topic, 'upset');
        $this->addWordWithRelation($topic, 'frightened');
        $this->addWordWithRelation($topic, 'scared');
        $this->addWordWithRelation($topic, 'exhausted');
        $this->addWordWithRelation($topic, 'bored');
        $this->addWordWithRelation($topic, 'guilty');
        $this->addWordWithRelation($topic, 'shocked');
        $this->addWordWithRelation($topic, 'frustrated');
        $this->addWordWithRelation($topic, 'excited');
        $this->addWordWithRelation($topic, 'delighted');
        $this->addWordWithRelation($topic, 'angry');
        $this->addWordWithRelation($topic, 'annoyed');
        $this->addWordWithRelation($topic, 'confused');
        $this->addWordWithRelation($topic, 'depressed');
        $this->addWordWithRelation($topic, 'disappointed');
        $this->addWordWithRelation($topic, 'embarrassed');
        $this->addWordWithRelation($topic, 'frustrated');
        $this->addWordWithRelation($topic, 'horrified');
        $this->addWordWithRelation($topic, 'irritated');
        $this->addWordWithRelation($topic, 'jealous');
        $this->addWordWithRelation($topic, 'joyful');
        $this->addWordWithRelation($topic, 'nervous');
        $this->addWordWithRelation($topic, 'outraged');
        $this->addWordWithRelation($topic, 'pleased');
        $this->addWordWithRelation($topic, 'proud');
        $this->addWordWithRelation($topic, 'relieved');
        $this->addWordWithRelation($topic, 'terrified');

        // Reactions
        $topic = DB::select('select id from topics where name = "reactions" limit 1');
        $this->addWordWithRelation($topic, 'wedding');
        $this->addWordWithRelation($topic, 'disease');
        $this->addWordWithRelation($topic, 'promotion');
        $this->addWordWithRelation($topic, 'engagement');
        $this->addWordWithRelation($topic, 'accident');
        $this->addWordWithRelation($topic, 'dismissal');
        $this->addWordWithRelation($topic, 'funeral');
        $this->addWordWithRelation($topic, 'graduation');
        $this->addWordWithRelation($topic, 'victory');
        $this->addWordWithRelation($topic, 'birth');
        $this->addWordWithRelation($topic, 'purchase');
        $this->addWordWithRelation($topic, 'retirement');
        $this->addWordWithRelation($topic, 'injury');
        $this->addWordWithRelation($topic, 'pregnancy');
        $this->addWordWithRelation($topic, 'relocation');
        $this->addWordWithRelation($topic, 'anniversary');
        $this->addWordWithRelation($topic, 'defeat');

        // Roles
        $topic = DB::select('select id from topics where name = "roles" limit 1');
        $this->addWordWithRelation($topic, 'parent');
        $this->addWordWithRelation($topic, 'teacher');
        $this->addWordWithRelation($topic, 'supervisor');
        $this->addWordWithRelation($topic, 'citizen');
        $this->addWordWithRelation($topic, 'preacher');
        $this->addWordWithRelation($topic, 'believer');
        $this->addWordWithRelation($topic, 'child');
        $this->addWordWithRelation($topic, 'patient');
        $this->addWordWithRelation($topic, 'friend');
        $this->addWordWithRelation($topic, 'collector');
        $this->addWordWithRelation($topic, 'homeowner');
        $this->addWordWithRelation($topic, 'driver');
        $this->addWordWithRelation($topic, 'husband');
        $this->addWordWithRelation($topic, 'wife');
        $this->addWordWithRelation($topic, 'boss');
        $this->addWordWithRelation($topic, 'sibling');
        $this->addWordWithRelation($topic, 'expert');
        $this->addWordWithRelation($topic, 'student');
        $this->addWordWithRelation($topic, 'fan');
        $this->addWordWithRelation($topic, 'employee');
        $this->addWordWithRelation($topic, 'sportsman');
        $this->addWordWithRelation($topic, 'customer');
        $this->addWordWithRelation($topic, 'neighbour');
        $this->addWordWithRelation($topic, 'advisor');

        // Believer
        $topic = DB::select('select id from topics where name = "believer" limit 1');
        $this->addWordWithRelation($topic, 'astrology');
        $this->addWordWithRelation($topic, 'magic');
        $this->addWordWithRelation($topic, 'alien');
        $this->addWordWithRelation($topic, 'afterlife');
        $this->addWordWithRelation($topic, 'mind');
        $this->addWordWithRelation($topic, 'fate');
        $this->addWordWithRelation($topic, 'destiny');
        $this->addWordWithRelation($topic, 'evolution');
        $this->addWordWithRelation($topic, 'psychic');
        $this->addWordWithRelation($topic, 'ability');
        $this->addWordWithRelation($topic, 'karma');
        $this->addWordWithRelation($topic, 'soulmates');
        $this->addWordWithRelation($topic, 'reincarnation');
        $this->addWordWithRelation($topic, 'ghost');
        $this->addWordWithRelation($topic, 'belief');

        // Whether
        $topic = DB::select('select id from topics where name = "whether" limit 1');
        $this->addWordWithRelation($topic, 'boiling');
        $this->addWordWithRelation($topic, 'chilly');
        $this->addWordWithRelation($topic, 'clear');
        $this->addWordWithRelation($topic, 'cloudy');
        $this->addWordWithRelation($topic, 'cold');
        $this->addWordWithRelation($topic, 'cool');
        $this->addWordWithRelation($topic, 'damp');
        $this->addWordWithRelation($topic, 'drizzle');
        $this->addWordWithRelation($topic, 'dry');
        $this->addWordWithRelation($topic, 'foggy');
        $this->addWordWithRelation($topic, 'freezing');
        $this->addWordWithRelation($topic, 'frosty');
        $this->addWordWithRelation($topic, 'hail');
        $this->addWordWithRelation($topic, 'hot');
        $this->addWordWithRelation($topic, 'humid');
        $this->addWordWithRelation($topic, 'icy');
        $this->addWordWithRelation($topic, 'lightning');
        $this->addWordWithRelation($topic, 'mild');
        $this->addWordWithRelation($topic, 'rainbow');
        $this->addWordWithRelation($topic, 'rainy');
        $this->addWordWithRelation($topic, 'shower');
        $this->addWordWithRelation($topic, 'sleet');
        $this->addWordWithRelation($topic, 'snowy');
        $this->addWordWithRelation($topic, 'stormy');
        $this->addWordWithRelation($topic, 'sunny');
        $this->addWordWithRelation($topic, 'thunderstorm');
        $this->addWordWithRelation($topic, 'warm');
        $this->addWordWithRelation($topic, 'wet');
        $this->addWordWithRelation($topic, 'windy');
        $this->addWordWithRelation($topic, 'forecast');

        // Disasters
        $topic = DB::select('select id from topics where name = "disasters" limit 1');
        $this->addWordWithRelation($topic, 'avalanche');
        $this->addWordWithRelation($topic, 'eruption');
        $this->addWordWithRelation($topic, 'flood');
        $this->addWordWithRelation($topic, 'earthquake');
        $this->addWordWithRelation($topic, 'hurricane');
        $this->addWordWithRelation($topic, 'tsunami');
        $this->addWordWithRelation($topic, 'tornado');
        $this->addWordWithRelation($topic, 'drought');
        $this->addWordWithRelation($topic, 'landslide');
        $this->addWordWithRelation($topic, 'predict');
        $this->addWordWithRelation($topic, 'heal');
        $this->addWordWithRelation($topic, 'sense');

        //
    }

    private function addWordWithRelation($topic, $word)
    {
        DB::table('words')->insert([
            'name'     => $word,
            'topic_id' => $topic[0]->id,
        ]);
    }
}
