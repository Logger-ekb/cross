<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('topics')->insert(['name' => 'feelings']);
        DB::table('topics')->insert(['name' => 'reactions']);
        DB::table('topics')->insert(['name' => 'roles']);
        DB::table('topics')->insert(['name' => 'believer']);
        DB::table('topics')->insert(['name' => 'whether']);
        DB::table('topics')->insert(['name' => 'disasters']);
    }
}
