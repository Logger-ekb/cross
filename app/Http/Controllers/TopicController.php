<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;

class TopicController
{
    public function get()
    {
        return Topic::selectRaw('id, name')->get();
    }

    public function add(Request $request)
    {
        Topic::create(['name' => $request->get('name')]);
        return response()->json(['success' => true]);
    }
}
