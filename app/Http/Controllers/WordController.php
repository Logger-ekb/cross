<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Services\MatrixService;
use Illuminate\Http\Request;

class WordController
{
    /** @var MatrixService */
    protected $matrixService;

    public function __construct(MatrixService $matrixService)
    {
        $this->matrixService = $matrixService;
    }

    public function getByTopic(Topic $topic, Request $request)
    {
        return response()->json($this->matrixService->getMatrix($topic, $request->get('size', 15)));
    }
}
