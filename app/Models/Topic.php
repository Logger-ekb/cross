<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Topic
 * @package App\Models
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $word
 * @property-read Collection|Word[] $words
 */
class Topic extends Model
{
    public function words()
    {
        return $this->hasMany(Word::class);
    }
}
