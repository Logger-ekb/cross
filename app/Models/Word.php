<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property-read Topic $topic
 */
class Word extends Model
{
    protected $hidden = ['pivot'];

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
}
