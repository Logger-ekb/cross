<?php


namespace App\Services;


use App\Models\Topic;
use App\Models\Word;
use Illuminate\Support\Facades\DB;

class MatrixService
{
    const DIRECTIONS = [
        'x', // horizontal
        'y', // vertical
        'xt', // diagonal from left top to right bottom
        'xb', // diagonal from right top to left bottom
    ];

    private $matrix = [];
    private $matrixLengthByX = 0;
    private $matrixLengthByY = 0;
    private $reindex = true;

    public function getMatrix(Topic $topic, int $size): array
    {
        $words = $topic->words()
            ->selectRaw('UPPER(name) as name')
            ->orderByRaw(DB::raw('RAND()'))
            ->take(10)
            ->get()->toArray();

        $words = array_map(function ($w) { return $w['name']; }, $words);
        // Some words should be flipped
        for ($i = 0; $i < \count($words); $i++) {
            $words[$i] = (bool) rand(0, 1)
                ? ['word' => $words[$i],          'flip' => false]
                : ['word' => \strrev($words[$i]), 'flip' => true];
        }

        if ($this->checkWordsLength($words, $size, $size)) {
            return [
                'message' => 'Chosen topic has word which more long than table length. Choose bigger size for table'
            ];
        }

        while ($this->reindex) {
            $this->createMatrix($size);
            $words = $this->addWordsToMatrix($words);
        }

        $this->fillEmptyCells();

        return [
            'matrix' => $this->matrix,
            'words'  => $words,
        ];
    }

    private function createMatrix(int $size = 15): void
    {
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size; $j++) {
                $this->matrix[$i][$j] = '';
            }
        }
    }

    private function addWordsToMatrix(array $words): array
    {
        $this->reindex = false;
        $directionsCount = \count(self::DIRECTIONS);

        for ($i = 0; $i < \count($words); $i++) {
            $words[$i]['x'] = [];
            $words[$i]['y'] = [];
            $word = $words[$i]['word'];
            $wordLength = \mb_strlen($word);
            $coordinates = $this->randCoordinates($wordLength, $directionsCount);

            if ($coordinates['direction'] === self::DIRECTIONS[0]) {
                $numberOfLetter = 0;
                for ($x = $coordinates['x']; $x < ($wordLength + $coordinates['x']); $x++) {
                    if ($this->matrix[$coordinates['y']][$x] != '' && $this->matrix[$coordinates['y']][$x] != $word[$numberOfLetter]) {
                        $this->reindex = true;
                        break;
                    } else {
                        $words[$i]['x'][] = $x;
                        $words[$i]['y'][] = $coordinates['y'];
                        $this->matrix[$coordinates['y']][$x] = $word[$numberOfLetter];
                        $numberOfLetter++;
                    }
                }
            }

            if ($coordinates['direction'] === self::DIRECTIONS[1]) {
                $numberOfLetter = 0;
                for ($y = $coordinates['y']; $y < ($wordLength + $coordinates['y']); $y++) {
                    if ($this->matrix[$y][$coordinates['x']] != '' && $this->matrix[$y][$coordinates['x']] != $word[$numberOfLetter]) {
                        $this->reindex = true;
                        break;
                    } else {
                        $words[$i]['y'][] = $y;
                        $words[$i]['x'][] = $coordinates['x'];
                        $this->matrix[$y][$coordinates['x']] = $word[$numberOfLetter];
                        $numberOfLetter++;
                    }
                }
            }

            if ($coordinates['direction'] === self::DIRECTIONS[2]) {
                $numberOfLetter = 0;
                for ($y = $coordinates['y']; $y < ($wordLength + $coordinates['y']); $y++) {
                    if (
                        $this->matrix[$y][$coordinates['x'] + $numberOfLetter] != ''
                        && $this->matrix[$y][$coordinates['x'] + $numberOfLetter] != $word[$numberOfLetter]
                    ) {
                        $this->reindex = true;
                        break;
                    } else {
                        $words[$i]['y'][] = $y;
                        $words[$i]['x'][] = $coordinates['x'] + $numberOfLetter;
                        $this->matrix[$y][$coordinates['x'] + $numberOfLetter] = $word[$numberOfLetter];
                        $numberOfLetter++;
                    }
                }
            }

            if ($coordinates['direction'] === self::DIRECTIONS[3]) {
                $numberOfLetter = 0;
                for ($y = ($wordLength + $coordinates['y']); $y > $coordinates['y']; $y--) {
                    if (
                        $this->matrix[$y][$coordinates['x'] + $numberOfLetter] != ''
                        && $this->matrix[$y][$coordinates['x'] + $numberOfLetter] != $word[$numberOfLetter]
                    ) {
                        $this->reindex = true;
                        break;
                    } else {
                        $words[$i]['y'][] = $y;
                        $words[$i]['x'][] = $coordinates['x'] + $numberOfLetter;
                        $this->matrix[$y][$coordinates['x'] + $numberOfLetter] = $word[$numberOfLetter];
                        $numberOfLetter++;
                    }
                }
            }
        }

        return $words;
    }

    private function randCoordinates(int $wordLength, int $directionsCount): array
    {
        $direction = \rand(0, $directionsCount - 1);

        switch ($direction) {
            case 0:
                $startPositionByXMustBeLessThen = $this->getMatrixLengthByX() - $wordLength;
                $startPositionByYMustBeLessThen = $this->getMatrixLengthByY();
                break;
            case 1:
                $startPositionByYMustBeLessThen = $this->getMatrixLengthByY() - $wordLength;
                $startPositionByXMustBeLessThen = $this->getMatrixLengthByX();
                break;
            default:
                $startPositionByXMustBeLessThen = $this->getMatrixLengthByX() - $wordLength;
                $startPositionByYMustBeLessThen = $this->getMatrixLengthByY() - $wordLength;
        }

        return [
            'x'         => rand(0, $startPositionByXMustBeLessThen - 1),
            'y'         => rand(0, $startPositionByYMustBeLessThen - 1),
            'direction' => self::DIRECTIONS[$direction],
        ];
    }

    private function fillEmptyCells(): void
    {
        for ($i = 0; $i < $this->getMatrixLengthByX(); $i++) {
            for ($j = 0; $j < $this->getMatrixLengthByY(); $j++) {
                if ($this->matrix[$i][$j] == '') {
                    $this->matrix[$i][$j] = \chr(\rand(65,90));;
                }
            }
        }
    }

    private function getMatrixLengthByX(): int
    {
        if ($this->matrixLengthByX === 0) {
            $this->matrixLengthByX = \count($this->matrix[0]);
        }

        return $this->matrixLengthByX;
    }

    private function getMatrixLengthByY(): int
    {
        if ($this->matrixLengthByY === 0) {
            $this->matrixLengthByY = \count($this->matrix);
        }

        return $this->matrixLengthByY;
    }

    private function checkWordsLength(array $words, int $sizeX, int $sizeY): bool
    {
        $isLongerThanMatrix = false;
        foreach ($words as $word) {
            $word = $word['word'];
            $wordLength = \mb_strlen($word);
            if ($sizeX < $wordLength || $sizeY < $wordLength) {
                $isLongerThanMatrix = true;
            }
        }

        return $isLongerThanMatrix;
    }
}
