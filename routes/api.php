<?php

use Illuminate\Support\Facades\Route;

Route::get('/topics', 'TopicController@get')->name('topics.all');
Route::post('/topics/add', 'TopicController@add')->name('topics.add');

Route::get('/words/{topic}', 'WordController@getByTopic')->name('words.get');
Route::post('/words/add', 'WordController@add')->name('words.add');
