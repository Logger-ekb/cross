<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="/css/bootstrap-grid.min.css" rel="stylesheet">
    <link href="https://unpkg.com/primevue/resources/themes/nova-light/theme.css " rel="stylesheet">
    <link href="https://unpkg.com/primevue/resources/primevue.min.css " rel="stylesheet">
    <link href="https://unpkg.com/primeicons/primeicons.css " rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700|Material+Icons' rel="stylesheet">-->
</head>
<body>
<div id="app">
    <app></app>
</div>
</body>
</html>
