export const getTopics = '/api/topics';
export const getWordsByTopic = (topicId) => `/api/words/${topicId}`;
export const addTopic = '/topics/add';
export const addWord = '/words/add';
