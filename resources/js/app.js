import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';
import AutoComplete from 'primevue/autocomplete';
import MultiSelect from 'primevue/multiselect';
import ProgressSpinner from 'primevue/progressspinner';
import OverlayPanel from 'primevue/overlaypanel';
import Dialog from 'primevue/dialog';

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.component('InputText', InputText);
Vue.component('Button', Button);
Vue.component('AutoComplete', AutoComplete);
Vue.component('MultiSelect', MultiSelect);
Vue.component('ProgressSpinner', ProgressSpinner);
Vue.component('OverlayPanel', OverlayPanel);
Vue.component('Dialog', Dialog);

import App from './App'
import Home from './Home'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
